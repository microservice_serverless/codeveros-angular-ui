import {
  MatButtonModule, MatDialogModule,
  MatIconModule,
  MatListModule, MatMenuModule,
  MatSidenavModule, MatTabsModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';
import { NgModule } from '@angular/core';
import { FlexModule } from '@angular/flex-layout';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {ConfirmDialogComponent} from './confirm-dialog/confirm-dialog.component';

const mdModules = [
  MatButtonModule,
  MatDialogModule,
  MatListModule,
  MatMenuModule,
  MatIconModule,
  MatSidenavModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule
];

const exportedModules = [
  BrowserAnimationsModule,
  CommonModule,
  FlexModule,
  RouterModule,
  ...mdModules
];

@NgModule({
  imports: [ ...exportedModules ],
  exports: [ ...exportedModules ],
  declarations: [ ConfirmDialogComponent ],
  entryComponents: [ ConfirmDialogComponent ]
})
export class SharedModule {}
