/*
 * Public API Surface of catalog
 */

export * from './lib/training.module';
export * from './lib/training.interface';
export * from './lib/training.config';
